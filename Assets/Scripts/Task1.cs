using UnityEngine;
using Random = System.Random;

public class Task1 : MonoBehaviour
{
    public int pointsCount;
    private Vector3[] _points;
    private int _currentPointIndex;
    private bool _forward = true;

    // Start is called before the first frame update
    private void Start()
    {
        FillPointsArray();
        transform.LookAt(_points[_currentPointIndex]);
    }

    // Update is called once per frame
    private void Update()
    {
        transform.position = Vector3.MoveTowards(
            transform.position,
            _points[_currentPointIndex],
            Time.deltaTime
        );

        if (transform.position != _points[_currentPointIndex]) return;

        if ((_forward && _currentPointIndex == pointsCount - 1) || (!_forward && _currentPointIndex == 0))
            _forward = !_forward;

        if (_forward)
            _currentPointIndex++;
        else
            _currentPointIndex--;

        transform.LookAt(_points[_currentPointIndex]);
    }

    private void FillPointsArray()
    {
        _points = new Vector3[pointsCount];
        var random = new Random();

        for (var i = 0; i < pointsCount; i++)
            _points[i] = new Vector3(random.Next(0, 5), random.Next(0, 5), random.Next(0, 5));
    }
}