using System.Collections;
using UnityEngine;

public class Task2 : MonoBehaviour
{
    public GameObject[] runners;
    public float passDistance;
    private int _currentRunnerIndex;

    private GameObject _firstRunner;
    private Vector3 _secondRunnerPosition;
    private bool _needTakePosition;

    private void Update()
    {
        runners[_currentRunnerIndex].transform.position = Vector3.MoveTowards(
            GetCurrentRunner().transform.position,
            GetNextRunner().transform.position,
            Time.deltaTime
        );

        if (_needTakePosition)
        {
            _firstRunner.transform.position = Vector3.MoveTowards(
                _firstRunner.transform.position,
                _secondRunnerPosition,
                Time.deltaTime
            );

            if (_firstRunner.transform.position == _secondRunnerPosition)
            {
                _needTakePosition = false;
            }
        }

        if (GetDistance() > passDistance) return;

        _firstRunner = GetCurrentRunner();
        _secondRunnerPosition = GetNextRunner().transform.position;
        MoveToPositionOfPreviousRunner();

        _currentRunnerIndex = GetNextRunnerIndex();
        GetCurrentRunner().transform.LookAt(GetNextRunner().transform.position);
    }

    private void MoveToPositionOfPreviousRunner()
    {
        StartCoroutine(WaitOneSecond());
    }

    private IEnumerator WaitOneSecond()
    {
        yield return new WaitForSeconds(1.5f);
        _needTakePosition = true;
    }

    private GameObject GetNextRunner()
    {
        return _currentRunnerIndex < runners.Length - 1 ? runners[_currentRunnerIndex + 1] : runners[0];
    }

    private int GetNextRunnerIndex()
    {
        return _currentRunnerIndex < runners.Length - 1 ? _currentRunnerIndex + 1 : 0;
    }

    private GameObject GetCurrentRunner()
    {
        return runners[_currentRunnerIndex];
    }

    private float GetDistance()
    {
        return Vector3.Distance(GetCurrentRunner().transform.position, GetNextRunner().transform.position);
    }
}